package utils

import "testing"

func TestDecodeImage(t *testing.T) {
	type args struct {
		bs64 string
		dest string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := DecodeImage(tt.args.bs64, tt.args.dest); (err != nil) != tt.wantErr {
				t.Errorf("DecodeImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
