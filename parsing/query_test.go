package parsing

import (
	"testing"
)

func TestUpdateFromAnonymousStruct(t *testing.T) {
	type AnonymousStruct struct {
		Jalan   string `db:"jalan" json:"jalan,omitempty"`
		KodePos string `db:"kode_pos" json:"kode_pos,omitempty"`
	}
	type TestCase struct {
		AnonymousStruct
		Name string `db:"name" json:"name,omitempty"`
	}
	var testcase1, testcase2 TestCase
	testcase1.Jalan = "nama jalan"
	testcase1.Name = "nama"
	testcase1.KodePos = "34162"
	testcase2.Jalan = "wkwk"
	testcase2.Name = "nama2"

	type args struct {
		data       interface{}
		structName string
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			"Benar",
			args{
				data:       testcase1,
				structName: "AnonymousStruct",
			},
			`jalan="nama jalan",kode_pos="34162"`,
		},
		{
			"Salah",
			args{
				data:       testcase2,
				structName: "AnonymousStruct",
			},
			`jalan="wkwk"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UpdateFromAnonymousStruct(tt.args.data, tt.args.structName); got != tt.want {
				t.Errorf("UpdateFromAnonymousStruct() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUpdateQuery(t *testing.T) {
	type args struct {
		data                interface{}
		withAnonymousStruct bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UpdateQuery(tt.args.data, tt.args.withAnonymousStruct); got != tt.want {
				t.Errorf("UpdateQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenerateDuplicateQuery(t *testing.T) {
	type args struct {
		data               interface{}
		isIgnoreEmptyValue bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GenerateDuplicateQuery(tt.args.data, tt.args.isIgnoreEmptyValue); got != tt.want {
				t.Errorf("GenerateDuplicateQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}
