package parsing

import (
	"fmt"
	"reflect"
	"strings"
)

type (
	DataTable struct {
		Draw    int `json:"draw"`
		Columns []struct {
			Data       interface{} `json:"data"`
			Name       string      `json:"name"`
			Searchable bool        `json:"searchable"`
			Orderable  bool        `json:"orderable"`
			Search     struct {
				Value string `json:"value"`
				Regex bool   `json:"regex"`
			} `json:"search"`
		} `json:"columns"`
		Order []struct {
			Column int    `json:"column"`
			Dir    string `json:"dir"`
		} `json:"order"`
		Start  int `json:"start"`
		Length int `json:"length"`
		Search struct {
			Value string `json:"value"`
			Regex bool   `json:"regex"`
		}
	}
)

// InsertQuery is method for get dynamic query key & value from request json data (struct) for insert data
func InsertQuery(data interface{}, table string) string {
	var columns, values []string
	ref := reflect.ValueOf(data)
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		field := ref.Field(i)
		val := field.Interface()
		isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
		if !isEmptyValue && key != "" {
			format := `"%v"`
			if reflect.TypeOf(val).Kind() == reflect.Ptr {
				val = field.Elem().Interface()
				if reflect.TypeOf(val).Kind() == reflect.Bool {
					format = "%v"
				}
			}
			columns = append(columns, fmt.Sprintf(`%s`, key))
			values = append(values, fmt.Sprintf(format, val))
		}
	}

	column := strings.Join(columns, ",")
	value := strings.Join(values, ",")
	query := fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", table, column, value)
	return query
}

// UpdateQuery is method for get dynamic query key & value from request json data (struct) for update data
func UpdateQuery(data interface{}, withAnonymousStruct bool) string {
	var params []string
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		field := ref.Field(i)
		key := typeOfData.Field(i).Tag.Get("db")
		isEmbedded := typeOfData.Field(i).Anonymous
		if isEmbedded && withAnonymousStruct {
			childInterface := ref.Field(i).Interface()
			childStruct := reflect.ValueOf(childInterface)
			childStructType := childStruct.Type()
			for i := 0; i < childStruct.NumField(); i++ {
				field = childStruct.Field(i)
				key = childStructType.Field(i).Tag.Get("db")
				if key == "" {
					continue
				}
				if val := constructUpdateValue(field, key); val != "" {
					params = append(params, val)
				}
			}
		} else {
			if key == "" {
				continue
			}
			if val := constructUpdateValue(field, key); val != "" {
				params = append(params, val)
			}
		}
	}
	param := strings.Join(params, ",")
	return param
}

func UpdateFromAnonymousStruct(data interface{}, structName string) string {
	var params []string
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		field := ref.Field(i)
		key := typeOfData.Field(i).Tag.Get("db")
		isEmbedded := typeOfData.Field(i).Anonymous
		if isEmbedded && structName == typeOfData.Field(i).Name {
			childInterface := ref.Field(i).Interface()
			childStruct := reflect.ValueOf(childInterface)
			childStructType := childStruct.Type()
			for i := 0; i < childStruct.NumField(); i++ {
				field = childStruct.Field(i)
				key = childStructType.Field(i).Tag.Get("db")
				if key == "" {
					continue
				}
				if val := constructUpdateValue(field, key); val != "" {
					params = append(params, val)
				}
			}
		}
	}
	param := strings.Join(params, ",")
	return param
}

// func InsertValueQuery(data interface{}) string {
// 	var values []string
// 	ref := reflect.ValueOf(data)
// 	for i := 0; i < ref.NumField(); i++ {
// 		field := ref.Field(i)
// 		val := field.Interface()
// 		isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
// 		if !isEmptyValue {
// 			format := `"%v"`
// 			if reflect.TypeOf(val).Kind() == reflect.Ptr {
// 				val = field.Elem().Interface()
// 				if reflect.TypeOf(val).Kind() == reflect.Bool {
// 					format = "%v"
// 				}
// 			}
// 			values = append(values, fmt.Sprintf(format, val))
// 		}
// 	}
// 	value := strings.Join(values, ",")
// 	return fmt.Sprintf("(%s)", value)
// }

// InsertDuplicateQuery for generate query string (INSERT INTO ... ON DUPLICATE KEY UPDATE) from type model Go struct
func InsertDuplicateQuery(data interface{}, table string) string {
	if data == nil {
		return ""
	}
	var columns, values, keyUpdates []string
	isBatchValues := true
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Slice {
		isBatchValues = false
		// for i := 0; i < ref.Len(); i++ {
		// 	value := InsertValueQuery(ref.Index(i).Interface())
		// 	values = append(values, value)
		// }
		ref = ref.Index(0)
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		field := ref.Field(i)
		val := field.Interface()
		isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
		if !isEmptyValue && key != "" {
			format := `"%v"`
			if reflect.TypeOf(val).Kind() == reflect.Ptr {
				val = field.Elem().Interface()
				if reflect.TypeOf(val).Kind() == reflect.Bool {
					format = `%v`
				}
			}
			if isBatchValues {
				values = append(values, fmt.Sprintf(format, val))
			} else {
				values = append(values, "?")
			}
			columns = append(columns, fmt.Sprintf("%s", key))
			keyUpdates = append(keyUpdates, fmt.Sprintf("%s=VALUES(%s)", key, key))
		}
	}
	column, value, keyUpdate := strings.Join(columns, ","), strings.Join(values, ","), strings.Join(keyUpdates, ",")
	query := fmt.Sprintf(`INSERT INTO %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s`, table, column, value, keyUpdate)
	return query
}

func InsertQueryTemplate(data interface{}, table string, includeDuplicateUpdate bool) string {
	if data == nil {
		return "" //, fmt.Errorf("Data is nil")
	}
	var columns, values, keyUpdates []string
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Slice {
		ref = ref.Index(0)
	}
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		// field := ref.Field(i)
		// val := field.Interface()
		// isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
		if key == "" || strings.Contains(key, "buff") {
			// if reflect.TypeOf(val).Kind() == reflect.Ptr {
			// 	val = field.Elem().Interface()
			// }
			continue
		}
		values = append(values, "?")
		columns = append(columns, fmt.Sprintf("%s", key))
		keyUpdates = append(keyUpdates, fmt.Sprintf("%s=VALUES(%s)", key, key))
	}
	column, value := strings.Join(columns, ","), strings.Join(values, ",")
	var duplicateUpdate string
	if includeDuplicateUpdate {
		keyUpdate := strings.Join(keyUpdates, ",")
		duplicateUpdate = fmt.Sprintf(`ON DUPLICATE KEY UPDATE %s`, keyUpdate)
	}
	query := fmt.Sprintf(`INSERT INTO %s (%s) VALUES (%s) %s`, table, column, value, duplicateUpdate)
	return query
}

func InsertQueryTemplateFromAnonymousStruct(data interface{}, table, structName string, includeDuplicateUpdate bool) string {
	if data == nil {
		return "" //, fmt.Errorf("Data is nil")
	}
	var columns, values, keyUpdates []string
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Slice {
		ref = ref.Index(0)
	}
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		fmt.Println(key)
		field := ref.Field(i)
		val := field.Interface()
		isEmbedded := typeOfData.Field(i).Anonymous
		if isEmbedded && structName == typeOfData.Field(i).Name {
			childInterface := ref.Field(i).Interface()
			childStruct := reflect.ValueOf(childInterface)
			childStructType := childStruct.Type()
			for i := 0; i < childStruct.NumField(); i++ {
				field = childStruct.Field(i)
				key = childStructType.Field(i).Tag.Get("db")
				if key == "" {
					continue
				}
				if reflect.TypeOf(val).Kind() == reflect.Ptr {
					val = field.Elem().Interface()
				}
				values = append(values, "?")
				columns = append(columns, fmt.Sprintf("%s", key))
				keyUpdates = append(keyUpdates, fmt.Sprintf("%s=VALUES(%s)", key, key))
			}
			break
		}
	}
	column, value := strings.Join(columns, ","), strings.Join(values, ",")
	var duplicateUpdate string
	if includeDuplicateUpdate {
		keyUpdate := strings.Join(keyUpdates, ",")
		duplicateUpdate = fmt.Sprintf(`ON DUPLICATE KEY UPDATE %s`, keyUpdate)
	}
	query := fmt.Sprintf(`INSERT INTO %s (%s) VALUES (%s) %s`, table, column, value, duplicateUpdate)
	return query
}

// GetContainValues for get not nil value from model struct. Return in array interface of data field
func GetContainValues(data interface{}) (res []interface{}) {
	ref := reflect.ValueOf(data)
	for i := 0; i < ref.NumField(); i++ {
		val := ref.Field(i).Interface()
		isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
		if !isEmptyValue {
			res = append(res, val)
		}
	}
	return
}

// GetValuesFromStruct for get not nil value from model struct. Return in array interface of data field
func GetValuesFromStruct(data interface{}) (res []interface{}) {
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		if key == "" || strings.Contains(key, "buff") {
			continue
		}
		val := ref.Field(i).Interface()
		res = append(res, val)
	}
	return
}

// GetValuesFromAnonymousStruct for get not nil value from model struct. Return in array interface of data field
func GetValuesFromAnonymousStruct(data interface{}, structName string) (res []interface{}) {
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		isEmbedded := typeOfData.Field(i).Anonymous
		if isEmbedded && structName == typeOfData.Field(i).Name {
			childInterface := ref.Field(i).Interface()
			childStruct := reflect.ValueOf(childInterface)
			childStructType := childStruct.Type()
			for i := 0; i < childStruct.NumField(); i++ {
				field := childStruct.Field(i)
				key := childStructType.Field(i).Tag.Get("db")
				if key == "" {
					continue
				}
				val := field.Interface()
				// isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
				// if isEmptyValue {
				// 	continue
				// }
				// fmt.Println(val)
				// if reflect.TypeOf(val).Kind() == reflect.Ptr {
				// 	val = field.Elem().Interface()
				// }
				res = append(res, val)
			}
		}
	}
	return
}

func GenerateDuplicateQuery(data interface{}, isIgnoreEmptyValue bool) string {
	if data == nil {
		return ""
	}
	var keyUpdates []string
	ref := reflect.ValueOf(data)
	if ref.Kind() == reflect.Slice {
		ref = ref.Index(0)
	}
	if ref.Kind() == reflect.Ptr {
		ref = reflect.ValueOf(data).Elem()
	}
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		field := ref.Field(i)
		val := field.Interface()
		isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
		if (isIgnoreEmptyValue && isEmptyValue) || strings.Contains(key, "buff") {
			continue
		}
		if field.Kind() == reflect.Ptr {
			if field.Elem().Kind() == reflect.Struct {
				continue
			}
		}
		if key == "" {
			continue
		}
		keyUpdates = append(keyUpdates, fmt.Sprintf("%s=VALUES(%s)", key, key))
	}
	keyUpdate := strings.Join(keyUpdates, ",")
	query := fmt.Sprintf(`ON DUPLICATE KEY UPDATE %s`, keyUpdate)
	return query
}

// SearchQuery is method for get query string from JSON OBJECT parameter for search data in database
func SearchQuery(data interface{}, tabel string) string {
	var params []string
	ref := reflect.ValueOf(data)
	typeOfData := ref.Type()
	for i := 0; i < ref.NumField(); i++ {
		key := typeOfData.Field(i).Tag.Get("db")
		field := ref.Field(i)
		val := field.Interface()
		isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
		if !isEmptyValue {
			var format string
			if len(fmt.Sprint(val)) > 3 {
				format = `%s.%s LIKE "%%%v%%"`
			} else {
				format = `%s.%s REGEXP "(^|[[:space:]])%v([[:space:]]|$)"`
			}
			params = append(params, fmt.Sprintf(format, tabel, key, val))
		}
	}
	param := strings.Join(params, " AND ")
	return param
}

// Not exported
// func constructInsertValue

func constructUpdateValue(value reflect.Value, key string) (result string) {
	format := `%s="%v"`
	val := value.Interface()
	isEmptyValue := reflect.DeepEqual(val, reflect.Zero(reflect.TypeOf(val)).Interface())
	if !isEmptyValue {
		if reflect.TypeOf(val).Kind() == reflect.Ptr {
			val = value.Elem().Interface()
			if reflect.TypeOf(val).Kind() == reflect.Bool {
				format = "%s=%v"
			}
		}
		if reflect.TypeOf(val).Kind() == reflect.Bool {
			format = "%s=%v"
		}
		result = fmt.Sprintf(format, key, val)
	}
	return
}
