module bitbucket.org/ofuro28/aimsis-backend-utils

go 1.12

require (
	github.com/anachronistic/apns v0.0.0-20151129191123-91763352f7bf
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/onsi/ginkgo v1.10.3 // indirect
	github.com/onsi/gomega v1.7.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/tealeg/xlsx v1.0.5
	gopkg.in/mailgun/mailgun-go.v1 v1.1.1
)
