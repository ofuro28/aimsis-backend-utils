package utils

import (
	"fmt"
	"net"
	"net/smtp"
	"regexp"
	"strings"
)

var (
	emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	// phoneRegexp = regexp.MustCompile(`(?:\+62)?0?8\d{2}(\d{8})`)
	phoneRegexp = regexp.MustCompile(`(?:\+62)?08\d{9,10}`)
)

func ValidateEmailFormat(email string) error {
	if !emailRegexp.MatchString(email) {
		return fmt.Errorf("Invalid Email Format")
	}
	return nil
}

func ValidatePhoneFormat(phone string) error {
	if !phoneRegexp.MatchString(phone) {
		return fmt.Errorf("Invalid Phone Number Format")
	}
	return nil
}

func ValidateEmailHost(email string) error {
	_, host := split(email)
	mx, err := net.LookupMX(host)
	if err != nil {
		return fmt.Errorf("Unresolvable Host")
	}

	client, err := smtp.Dial(fmt.Sprintf("%s:%d", mx[0].Host, 25))
	if err != nil {
		return err
	}
	defer client.Close()
	err = client.Hello("checkmail.me")
	if err != nil {
		return err
	}
	err = client.Mail("agungdwiprasetyo22@gmail.com")
	if err != nil {
		return err
	}
	err = client.Rcpt(email)
	if err != nil {
		return err
	}
	return nil
}

func split(email string) (account, host string) {
	i := strings.LastIndexByte(email, '@')
	account = email[:i]
	host = email[i+1:]
	return
}
