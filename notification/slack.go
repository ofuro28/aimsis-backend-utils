package notification

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"
)

var webhooks = map[string]string{
	"backend-error":  "https://hooks.slack.com/services/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",
	"backup-status":  "https://hooks.slack.com/services/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",
	"forget-account": "https://hooks.slack.com/services/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",
}

func SendToSlackChannel(channel, message string) error {
	url := webhooks[channel]
	if url == "" {
		return fmt.Errorf("Channel %s undefined", channel)
	}
	client := http.Client{}

	var payload struct {
		Text string `json:"text"`
	}
	payload.Text = message
	jsonPayload, _ := json.Marshal(payload)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonPayload))
	req.Header.Set("Content-Type", "application/json")
	_, err = client.Do(req)
	if err != nil {
		fmt.Println("Unable to reach the server.")
		return err
	}
	// body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println("body=", string(body))
	return nil
}

func BackendError(param ...interface{}) error {
	if os.Getenv("DB_SERVER") == "local" {
		return nil
	}
	var str []string
	for _, val := range param {
		str = append(str, fmt.Sprint(val))
	}
	message := strings.Join(str, " >>> ")
	message = strings.Replace(message, "\n", " ", -1)
	payload := fmt.Sprintf("*New Error Detected!*\n*Time*\t\t: %v\n*Message*\t: %s", time.Now(), message)
	if err := SendToSlackChannel("backend-error", payload); err != nil {
		return err
	}
	return nil
}
