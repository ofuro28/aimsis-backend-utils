package notification

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	apns "github.com/anachronistic/apns"
)

type (
	APNS struct {
		DeviceToken string
		Client      *apns.Client
	}

	GCM struct {
		RegistrationID []string `json:"registration_ids,omitempty"`
		Data           struct {
			Message    string `json:"message,omitempty"`
			Title      string `json:"title,omitempty"`
			Subtitle   string `json:"subtitle,omitempty"`
			TickerText string `json:"tickerText,omitempty"`
			Vibrate    int    `json:"vibrate,omitempty"`
			Sound      int    `json:"sound,omitempty"`
			LargeIcon  string `json:"largeIcon,omitempty"`
			SmallIcon  string `json:"smallIcon,omitempty"`
		} `json:"data,omitempty"`
	}
)

func NewAPNS(deviceToken string) *APNS {
	var apnsServer, cert, key string
	if os.Getenv("MODE") == "prod" {
		apnsServer = os.Getenv("APNS_SERVER_PROD")
		cert = os.Getenv("APP_DIR") + "/key/apns/apns-cert-prod.pem"
		key = os.Getenv("APP_DIR") + "/key/apns/apns-key-prod.pem"
	} else {
		apnsServer = os.Getenv("APNS_SERVER_DEV")
		cert = os.Getenv("APP_DIR") + "/key/apns/apns-cert-dev.pem"
		key = os.Getenv("APP_DIR") + "/key/apns/apns-key-dev.pem"
	}
	client := apns.NewClient(apnsServer, cert, key)
	return &APNS{DeviceToken: deviceToken, Client: client}
}

func NewGCM(deviceTokens ...string) *GCM {
	var gcm GCM
	gcm.RegistrationID = deviceTokens
	return &gcm
}

func (app *APNS) Send(message string) error {
	payload := apns.NewPayload()
	payload.Alert = message
	payload.Badge = 1

	pushNotif := apns.NewPushNotification()
	pushNotif.DeviceToken = app.DeviceToken
	pushNotif.AddPayload(payload)

	resp := app.Client.Send(pushNotif)
	alert, err := pushNotif.PayloadString()
	if err != nil {
		return err
	}
	fmt.Println("Alert:", alert)

	if !resp.Success {
		return fmt.Errorf("%s", resp.Error)
	}

	return nil
}

func (gcm *GCM) Send(message string) error {
	gcm.Data.Message = message
	reqPay, err := json.Marshal(gcm)
	if err != nil {
		return err
	}

	req, _ := http.NewRequest("POST", os.Getenv("GCM_SERVER"), bytes.NewBuffer(reqPay))
	req.Header.Set("Authorization", os.Getenv("GCM_AUTH"))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("%s %s", "Error send GCM", err.Error())
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("%s", "Something error when send request to GCM server")
	}
	return nil
}
