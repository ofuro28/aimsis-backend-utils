package notification

import (
	"fmt"
	"net/http"
	"os"

	mailgun "gopkg.in/mailgun/mailgun-go.v1"
)

type Notification struct {
	Receiver string
}

func NewTarget(receiver string) *Notification {
	return &Notification{Receiver: receiver}
}

func (email *Notification) SendEmail(subject, message string) error {
	mg := mailgun.NewMailgun(os.Getenv("MAILGUN_DOMAIN"), os.Getenv("MAILGUN_API_KEY"), os.Getenv("MAILGUN_PUBLIC_APIKEY"))
	newMessage := mailgun.NewMessage(os.Getenv("MAILGUN_EMAIL"), subject, message, email.Receiver)
	resp, id, err := mg.Send(newMessage)
	if err != nil {
		return fmt.Errorf("%s %s", "Error send email mailgun.", err.Error())
	}
	fmt.Printf("ID: %s Resp: %s\n", id, resp)
	return nil
}

func (sms *Notification) SendSMS(message string) error {
	req, _ := http.NewRequest("GET", os.Getenv("SMS_SERVER"), nil)
	uri := req.URL.Query()
	uri.Add("username", os.Getenv("SMS_USER"))
	uri.Add("password", os.Getenv("SMS_PASSWORD"))
	uri.Add("hp", sms.Receiver)
	uri.Add("message", message)
	req.URL.RawQuery = uri.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("%s %s", "Error send SMS.", err.Error())
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("%s", "Something Error in respons status")
	}
	return nil
}
