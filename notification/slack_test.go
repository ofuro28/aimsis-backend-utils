package notification

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChannelSlack(t *testing.T) {
	channel := "test"
	err := SendToSlackChannel(channel, "")
	assert.Equal(t, fmt.Errorf("Channel test undefined"), err)
}

func TestForgotAccountSlack(t *testing.T) {
	// channel := "forget-account"

}
