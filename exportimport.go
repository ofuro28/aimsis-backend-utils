package utils

import (
	"fmt"

	"github.com/tealeg/xlsx"
)

func WriteExcel(fileEx map[string]string, data interface{}) error {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	path := "./file/" + fileEx["filename"]

	file = xlsx.NewFile()
	sheet, err = file.AddSheet(fileEx["sheet1"])
	if err != nil {
		return err
	}
	row = sheet.AddRow()
	cell = row.AddCell()
	cell.Value = "[Registration data]"
	err = file.Save(path)
	if err != nil {
		return err
	}
	return nil
}

func ReadExcel() {
	excelFileName := "./tes.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Println(err.Error())
	}
	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			for _, cell := range row.Cells {
				text := cell.String()
				fmt.Printf("%s ", text)
			}
			fmt.Println("\n")
		}
	}
}
